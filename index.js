var fs = require('fs');
var path = require('path');
var spawn = require('child_process').spawn;
var temp = require('temp');
var util = require('util');

function error(exitCode, message) {
	if (message) process.stderr.write(message + '\n');
	process.exit(1);
}
function swapPackage(packageName) {
	var packageDir = path.join(".", "node_modules", packageName);

	if (!fs.existsSync(packageDir)) {
		return error(1, util.format(
			'package %s is not installed - install it first', packageName));
	}

	var packageJson = JSON.parse(fs.readFileSync(path.join(packageDir, "package.json")));
	if (!(packageJson && packageJson.repository && packageJson.repository.url)) {
		return error(1, util.format(
			'package %s does not specify a repository URL', packageName))
	}
	if (packageJson.repository.type !== 'git') {
		return error(1, util.format(
			'%s repositories are unsupported', packageJson.repository.type));
	}
	var repo = packageJson.repository.url;

	var tempDir = temp.mkdirSync({dir: 'node_modules', prefix:packageName + '.tmp'});
    fs.chmodSync(tempDir, 0777);
	var git = spawn('git', ['clone', repo, tempDir], {stdio: 'inherit'})

	git.on('close', function(code) {
		if (code !== 0) return error(1);
		var npm = spawn('npm', ['install', '.'], {cwd: tempDir, stdio: 'inherit'});
		npm.on('close', function(code) {
			if (code !== 0) return error(1);

			fs.renameSync(packageDir, packageDir + ".old");
			fs.renameSync(tempDir, packageDir);
			process.stdout.write(util.format("%s cloned to %s", packageName, packageDir));
		});
	});


}

module.exports = swapPackage;
